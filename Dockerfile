# docker build -t categories-back .
FROM python:3.9-alpine
WORKDIR /app
COPY pip.txt .
RUN pip install -r pip.txt
ADD *.py ./
ADD *.json ./
CMD ["python", "main.py"]