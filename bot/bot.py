#!/usr/bin/python -tt
# -*- coding: utf-8 -*-

# telegram modules
import logging
from telegram import Update, ForceReply, InputMediaPhoto
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, ChatMemberAdministrator
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
    CallbackQueryHandler
)
import requests
# other modules
import ast
import json


# Enable logging
logging.basicConfig(
    format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

updater = Updater('1914251472:AAGt41GHDpIPeTtiqs7k0enOfBQfmpu5Urk')


#########################################################################


def category(update: Update, context: CallbackContext) -> None:
    update_dict = ast.literal_eval(str(update))
    
    text = update_dict['message']['text']
    sheet_name = 'adva ua'

    r = requests.post("http://categories-back:7648/categorize", json={'text': text, 'sheet_name': sheet_name})

    ans = ast.literal_eval(str(r.text))

    answer = 'Категорія - ' +  ans['category'] + '\n\n'
    for cat in ans['all_categories']:
        answer += cat['category'] + ' (' + str(cat['score']) + ' входжень) - ' + str(cat['probability'] * 100) + '%\n'

    context.bot.send_message(chat_id = update.effective_chat.id,
                             text = answer)
    
    


def main() -> None:

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # default
    dispatcher.add_handler(MessageHandler(Filters.text | ~Filters.command, category))
    
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
