#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import (
    Flask,
    url_for,
    render_template,
    request,
    abort
)
from flask_pydantic import validate
from typing import Optional
from pydantic import BaseModel
import json
import os

# Google Apis
import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials


### config ###
app = Flask(__name__)

with open('config.json') as config_file:
    data = json.load(config_file)

host = data['host']
port = data['port']
spreadsheet_id = data['spreadsheet_id']
### end config ###

CREDENTIALS_FILE = os.path.dirname(__file__)  
credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.join(CREDENTIALS_FILE, 'google_creditionals.json'),
                                                               ["https://www.googleapis.com/auth/spreadsheets",
                                                                "https://www.googleapis.com/auth/spreadsheets.readonly",
                                                                "https://www.googleapis.com/auth/documents"])
httpAuth = credentials.authorize(httplib2.Http())
sheet_service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)

class CategorizeText(BaseModel):
    text: str
    sheet_name: str


@app.route('/categorize', methods=['POST'])
@validate()
def medicines(body: CategorizeText):
    
    request = sheet_service.spreadsheets().values().get(spreadsheetId = spreadsheet_id,
                                                        range = body.sheet_name).execute()

    text = body.text.lower()
    categories = request.get('values', [])[1:]
    counts = {}

    for line in categories:
        word = line[0]
        category = line[1]

        count = text.count(word)

        try:
            counts[category] += count
        except KeyError:
            counts[category] = count

    result = {'status': 'success', 'all_categories': []}
    total_count = 0
    max_count = 0
    result_category = 'Не визначено'
    
    for category in counts:
        total_count += counts[category]
        if max_count < counts[category]:
            result_category = category
            max_count = counts[category]

    result['category'] = result_category

    
    all_cat = ''
    
    for category in counts:
        item = {}
        item['category'] = category
        item['score'] = counts[category]
        if total_count != 0:
            item['probability'] = counts[category] / total_count
        else:
            item['probability'] = 0

        all_cat += item['category'] + ' - ' + str(item['probability'] * 100) + '%\n'
        
        result['all_categories'].append(item)

    all_cat = all_cat[:-1]

    row = {'values': [[text, all_cat, result_category]]}
        
    response = sheet_service.spreadsheets().values().append(spreadsheetId = spreadsheet_id,
                                                           range = 'logs',
                                                           valueInputOption = 'RAW',
                                                           body = row).execute()

    print(response)
    
    
    return result

if __name__ == '__main__':
    app.run(host, port)
